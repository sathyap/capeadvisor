<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
   Template Name: portfolio_sub	
 */

get_header(); ?>

		<div id="primary">
		<div id="content">
		<div class="bodyContainer">
		<div class="portfolio_content">				  
		  <div class="left_contents_one">		      	  
		  	<div class="custom_img"><a href="javascript:void(0);" ><img src="<?php the_field('image'); ?>"  width="" height="" alt="Slider Image"/></a></div>
		  <div class="align">
       <h1 class="entry-title_one"> <?php
$parent_title = get_the_title($post->post_parent);
echo $parent_title;
?></h1>  
<?php 
if(wp_list_pages('child_of='.$post->ID.'&echo=0&exclude=2')) {
echo "<ul>";
wp_list_pages('title_li=&child_of='.$post->ID.'&exclude=2');
echo "</ul>";
}
elseif(get_the_title($post->post_parent) != the_title(' ' , ' ',false)) {
echo "<ul>";
wp_list_pages('child_of='.$post->post_parent.'&title_li=&exclude=2');
echo "</ul>";
} ?>


    </div>
    <div class="left_contents_two">
	      
		  </div>
		  </div>  
		    <div class="right_contents">
		   <?php while ( have_posts() ) : the_post(); ?>

		<a href="#">   <?php the_post_thumbnail('full');  ?> </a>

				<?php endwhile; // end of the loop. ?>				
		    <div class="first_img one"></div>
		    		<div class="second_img one"></div>
		    		<div class="third_img one"></div>
		    		<div class="fourth_img one"></div>
		    		<div class="fifth_img one"></div>
		    		<div class="sixth_img one"></div>
		
		    </div>
		
		</div>
	
			</div>
			</div>
		</div><!-- #primary -->

<?php get_footer(); ?>
