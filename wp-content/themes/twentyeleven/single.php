<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */


get_header(); ?>
<?php $category = get_the_category($post->ID); ?>
		<div id="primary">
			<div id="content" role="main">
<div class="bodyContainer">
				<div class="leftContainer">
					<div id="profileleftbar">
						<div class="mediaDesc">
							<div class="leftSideNavigation">	<ul>							
          <?php query_posts('category_name=media');
         
        while (have_posts()) : the_post();
        $attachment_id = get_field("pdf_file");
        ?>
         
     <li>  <a href="<?php if(get_field("pdf_file")){echo $attachment_id[url];}else{ the_permalink(); } ?>" title="Permanent Link to <?php the_title(); ?>" target="<?php if($attachment_id[mime_type]=='application/pdf'){?> _blank <?php } ?>"><?php the_title();  ?> &nbsp;<?php echo the_time('m/y');?></a>
       </li>
        <?php
        endwhile; wp_reset_query(); 
        ?>
 <ul>
							</div>
						</div>
					</div>
					<div id="leftbarimg2"><img src="<?php bloginfo('template_url'); ?>/images/nimble.jpg" alt="nimble"/></div>
				</div>
				<div class="rightContainer" id="media_link">
				<div class="media_full_content">
					
						<h1><?php echo $category[0]->cat_name; ?></h1>
						
						
						<h2><?php the_title();?>, <span><?php echo get_the_date(); ?></span></h2>
						   <?php the_content();?> 
</div>
						
			
					
					
				</div>
			
			
			
			
			
			
				
			</div><!-- #content -->
		</div><!-- #primary -->
</div>

<?php get_footer(); ?>
