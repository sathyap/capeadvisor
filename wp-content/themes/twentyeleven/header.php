<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<meta name="description" content="Cape Advisors,real estate development and investment firm.Tel: (212) 343.1700,Fax:( 212) 343.8510">
<meta name="keywords" content="Imagination, creativity and vision,Experience in public private partnerships" />
<!--<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>-->
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/css/prettyPhoto.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/css/media.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/css/jquery.bxslider.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/css/jquery.jscrollpane.css" />
<script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js" ></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.prettyPhoto.js" ></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.bxslider.js" ></script>
<script src="<?php bloginfo('template_url'); ?>/js/slideshow.js" ></script>
<script src="<?php bloginfo('template_url'); ?>/js/screen.js" ></script>
<script src="<?php bloginfo('template_url'); ?>/js/script.js" ></script>
<script src="<?php bloginfo('template_url'); ?>/js/selectivizr-min.js" ></script>
<script src="<?php bloginfo('template_url'); ?>/js/css_browser_selector.js" ></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.jscrollpane.min.js" ></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.mousewheel.js" ></script>
<script type="text/javascript">
jQuery(window).bind("load", function() {
//var high = jQuery('.content .inner_content').height();

/*if ( jQuery.browser.msie && jQuery.browser.version==7.0 ) {
  setTimeout('setHeight()', 1000);
}else {
	setHeight();
} */
		
});

function setHeight() {
var high1=jQuery('.mediaDesc').height();
var higg2=jQuery('.media_full_content').height();
//var high=Math.max(high1 ,higg2);
var final_high = higg2 - 213;
jQuery('.mediaDesc').css('height',final_high+'px');
//jQuery('.media_full_content').css('height',high+'px');
}



</script>

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>

</head>

<?php if(is_front_page()) 
{ ?> <body class="HomeBody"> <?php } else ?> 


<body>
<div class="top_page"></div>
<div id="page">
	<header id="header">
		<div class="logo">
			<h1><a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Cape Advisor" title="Return to Cape Advisor home page" style="width:188px;height:36px;" /></a>
			</h1>
		</div>	
		<nav id="access" role="navigation">
		
		
		<a class="toggleMenu" href="#">Menu</a>
		
			<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
			
		</nav><!-- #access -->
		<br/>
	</header><!-- #branding -->
	<div id="main">
