<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
   Template Name: portfolio	
 */

get_header(); ?>

		<div id="primary test">
		<div id="content">
		<div class="bodyContainer">
		<div class="portfolio_content">				  
		  <?php get_sidebar();?>
		 <div class="right_contents">
		 <div class="right_contents_inner">
		   <?php while ( have_posts() ) : the_post(); ?>


<?php
//$child_pages = $wpdb->get_results("SELECT *    FROM $wpdb->posts WHERE post_parent = ".$post->ID." AND post_type = 'page' ORDER BY menu_order", 'OBJECT');    


 $args = array(
	'parent' => $post->ID, 'hierarchical' => 0, 'sort_column' => 'menu_order'
); 

$child_pages = get_pages($args);
$index=100;
?>
<?php if ( $child_pages ) : foreach ( $child_pages as $pageChild ) : setup_postdata( $pageChild ); ?>
<div class="child-thumb" style="z-index:<?php echo $index; ?>">
    <div class="child-inner">  
         <div class="first_img one"></div>
		    		<div class="second_img one"></div>
		    		<div class="third_img one"></div>
		    		<div class="fourth_img one"></div>
		    		<div class="fifth_img one"></div>
		    		<div class="sixth_img one"></div>
  
 <a href="#inline-<?php echo $pageChild->ID; ?>" rel="prettyPhoto" class="pretty_photo" title="<?php echo $pageChild->post_title; ?>"><?php echo get_the_post_thumbnail($pageChild->ID, 'full'); 
 
 $index--;
 ?></a>
    </div>
</div>

<div class="light_box_area" id="inline-<?php echo $pageChild->ID; ?>" style="display:none;">

   <div class="portfolio_contents">
			<h1><?php echo $pageChild->post_title; ?></h1>

<?php if(get_field( "hotel", $pageChild->ID )) { ?> 

	<p><?php echo get_field( "hotel", $pageChild->ID ); ?></p>
<?php } ?>

		        <?php if(get_field( "address", $pageChild->ID )) { ?> <p><?php echo get_field( "address", $pageChild->ID ); ?></p> <?php } ?>
				<?php if(get_field( "website", $pageChild->ID )) { ?> 
			 	<p><span>WEBSITE: </span><a target="_blank" href="<?php echo get_field( 'website', $pageChild->ID ); ?>"><?php echo get_field( 'website', $pageChild->ID ); ?></a></p> <?php } ?>
<?php if(get_field( "arcitect", $pageChild->ID )) { ?> 
			 	<p><span>ARCHITECT: </span><?php echo get_field( "arcitect", $pageChild->ID ); ?></p>
<?php } ?>

<?php if(get_field( "interior_design", $pageChild->ID )) { ?> 
			 	<p><span>INTERIOR DESIGN: </span><?php echo get_field( "interior_design", $pageChild->ID ); ?></p>
<?php } ?>

<?php if(get_field( "project_architect", $pageChild->ID )) { ?> 

	<p><span>PROJECT ARCHITECT: </span><?php echo get_field( "project_architect", $pageChild->ID ); ?></p>
<?php } ?>
<?php if(get_field( "design_architect", $pageChild->ID )) { ?> 

	<p><span>DESIGN ARCHITECT: </span><?php echo get_field( "design_architect", $pageChild->ID ); ?></p>
<?php } ?>
<?php if(get_field( "cape_advosors_role", $pageChild->ID )) { ?> 

	<p><span>CAPE ADVISORS ROLE: </span><?php echo get_field( "cape_advosors_role", $pageChild->ID ); ?></p>
<?php } ?>

<?php if(get_field( "architecture_design", $pageChild->ID )) { ?> 

	<p><span>ARCHITECTURE/DESIGN: </span><?php echo get_field( "architecture_design", $pageChild->ID ); ?></p>
<?php } ?>


			 	<p class="content"><?php echo get_the_content($pageChild->ID); ?></p>
<?php if(get_field( "manged_by", $pageChild->ID )) { ?> 
			 	<p><span>This hotel is managed by</span><br />
			 	<a target="_blank" href="<?php echo get_field( 'manged_by', $pageChild->ID ); ?>" class="manage"> <?php echo get_field( 'manged_by', $pageChild->ID ); ?></a> </p>  
<?php } ?>    
		</div>
    
  <div class="image-holder">
  
  <?php
  
   
$attachments = get_children(array('post_parent' => $pageChild->ID,
						'post_status' => 'inherit',
						'post_type' => 'attachment',
						'post_mime_type' => 'image',
						'order' => 'ASC','numberposts'    => -1,
						'orderby' => 'menu_order ID'));





foreach($attachments as $att_id => $attachment) {
	$portfolio_img = wp_get_attachment_image($attachment->ID, 'portfolio-medium');
     
        ?>
   
    
		<div class="WallSizesThumbHolder">
			<a href="#" title="<?php echo $attachment->post_title; ?>"><?php echo $portfolio_img; ?> </a>
		</div>
		
<?php
}
?>
</div>
</div>


<?php endforeach; endif;
?>		
				<?php endwhile; // end of the loop. ?>		
				
		 
		
		    </div>
		</div>
		</div>
	
			</div>
			</div>
			 <?php if ( function_exists( 'get_smooth_slider' ) ) {
get_smooth_slider(); }
?> 
		</div><!-- #primary -->
		
<?php get_footer(); ?>
