<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
   Template Name: team	
 */

get_header(); ?>
<style>
.rightbarimgs {background-color: #63708A;}
.rightbarimgs img {float:right;}
</style>
		<div id="primary">
			<div id="content" role="main">
<div class="bodyContainer">
					<div class="leftContainer">
					<div class="leftbars"><img src="<?php the_field('left_image_1', 14); ?>" alt="profile" /></div>
					<div class="leftbarimgs"><img src="<?php the_field('left_image_2', 14); ?>" alt="profile" /></div>
				</div>
				<div class="rightContainer">
					<div class="profileDesc tEam">
						<h1>Team</h1>		
						<ul class="heading">
				<?php if('publish' === get_post_status( 689 )) { ?>

						<li>The Cape Advisors Team:</li>
				<?php } ?>
				<?php if('publish' === get_post_status( 691 )) { ?>
				    <li>The Cape Resorts Team:</li>
				<?php } else { echo "<li style='visibility:hidden;'>TCape Resorts Team</li>"; }?>
				    </ul>
						<div class="first_set">
						<?php
$pageChildren = get_pages('child_of=689&sort_order=asc&sort_column=menu_order');

$i=0;
if ( $pageChildren ) {
  foreach ( $pageChildren as $pageChild ) {
 
 
 
 if($i<=6) {
 
 if($i == 0) { $initial = get_the_ID(); 
  echo '<div class="test1">';
 
 }
  $class = (get_the_ID() == $pageChild->ID || get_the_ID() == 689) ? "active2": "";
 
    echo '<div class="'.$class.'"><a href="' . get_permalink($pageChild->ID) . '">'. $pageChild->post_title.'</a></div>
';

 if($i == 6 || $i==(count($pageChildren)-1)) {
   
  echo '</div>';
 
 }
 }
 
 
else {

if($i==7) {
 
  echo '<div class="test2">';
 
}
  $class = (get_the_ID() == $pageChild->ID || get_the_ID() == 689) ? "active2": "";
    echo '<div class="'.$class.'"><a href="' . get_permalink($pageChild->ID) . '">'. $pageChild->post_title.'</a></div>
';

 if($i==(count($pageChildren)-1)) {
  echo '</div>';
 
 }
 
}
    
    $i++;
   
  }
}
?>	
</div>	
	<div class="second_set">	
	<?php
$pageChildren = get_pages('child_of=691');
$i=0;
if ( $pageChildren ) {
  foreach ( $pageChildren as $pageChild ) {
 
 
 
 if($i<=6) {
 
 if($i == 0) { 
  echo '<div class="test3">';
 
 }
  $class = (get_the_ID() == $pageChild->ID || get_the_ID() == 691) ? "active2": "";
 
    echo '<div class="'.$class.'"><a href="' . get_permalink($pageChild->ID) . '">'. $pageChild->post_title.'</a></div>
';

 if($i == 6 || $i==(count($pageChildren)-1)) {
   
  echo '</div>';
 
 }
 }
 
 
else {

if($i==7) {
 
  echo '<div class="test4">';
 
}
  $class = (get_the_ID() == $pageChild->ID || get_the_ID() == 691) ? "active2": "";
    echo '<div class="'.$class.'"><a href="' . get_permalink($pageChild->ID) . '">'. $pageChild->post_title.'</a></div>
';

 if($i==(count($pageChildren)-1)) {
  echo '</div>';
 
 }
 
}
    
    $i++;
   
  }
}
?>	
</div>			 				 			

					</div>
					<div class="rightbarOuter">
					<div class="rightbarimgs"><img src="<?php the_field('image', 14);?>" alt="profile" /></div>
					<div class="profileDesc1">
					<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', 'page' ); ?>
						<?php endwhile; // end of the loop. ?>
						<?php 
if(is_page('team')) { ?>

<?php
//$id=709; 
//$post = get_page($id); 
//$content = apply_filters('the_content', $post->post_content); 
//echo $content;
//the_content();  
}
?>

					</div>
				</div></div>
			</div><!-- #content -->
		</div><!-- #primary -->
</div>

<?php get_footer(); ?>
