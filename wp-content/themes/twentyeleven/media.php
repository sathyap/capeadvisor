<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
   Template Name: media	
 */

get_header(); ?>
<?php  	$page_title = get_the_title(); 	

?>
		<div id="primary">
			<div id="content" role="main">
<div class="bodyContainer">
				<div class="leftContainer">
					<div id="profileleftbar">
						<div class="mediaDesc">
							<div class="leftSideNavigation">	<ul>							
          <?php query_posts('category_name=media&posts_per_page=-1');
         
        while (have_posts()) : the_post();
       	$attachment_id = get_field("pdf_file");
        ?>
         
     <li>  <a href="<?php if(get_field("pdf_file")){echo $attachment_id[url];}else{ the_permalink(); } ?>" title="Permanent Link to <?php the_title(); ?>" target="<?php if($attachment_id[mime_type]=='application/pdf'){?> _blank <?php } ?>"><?php the_title();  ?> &nbsp;<?php echo the_time('m/y');?></a>
       </li>
        <?php
        endwhile; wp_reset_query(); 
        ?>
 <ul>
							</div>
						</div>
					</div>
					<div id="leftbarimg2"><img src="<?php bloginfo('template_url'); ?>/images/nimble.jpg" alt="nimble"/></div>
				</div>
				<div class="rightContainer">
					<div class="profileDesc" id="media">
						<h1><?php echo $page_title; ?></h1>
						    <?php 
              /*  query_posts('category_name = media'); 		
                if ( have_posts() ) while ( have_posts() ) : the_post();

                 endwhile; 
 
              
                dynamic_sidebar('latest posts'); */

                       ?>  

<aside id="super_recent_posts_widget-2" class="widget super_recent_posts">
	<div class="super_recent_posts_item media_first">
		<div class="post-entry"> <?php the_content(); ?>
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<!--<div class="super_recent_posts_item media_second">
		<div class="post-entry"> <?php	the_secondary_content();?>
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<div class="super_recent_posts_item media_third">
		<div class="post-entry">
			<?php echo the_field('extra_2'); ?>
		</div>
		<div class="clear">&nbsp;</div>
	</div>-->
</aside>


                        


						
					</div>
					<div class="mediaDiv">
					<div class="mediaInner">
					<div class="rightbarimg2" id="ri_img2">
						<div class="grayBG">
							<div class="super_recent_posts_item media_second">
		<div class="post-entry"> <?php	the_secondary_content();?>
		</div>
		<div class="clear">&nbsp;</div>
	</div>
						</div>
					</div>
					<div id="mediaDesc" class="desc">
						<div class="super_recent_posts_item media_third">
		<div class="post-entry">
			<?php echo the_field('extra_2'); ?>
		</div>
		<div class="clear">&nbsp;</div>
	</div>
	</div>
					</div>
				</div>
				</div>					
			</div><!-- #content -->
		</div><!-- #primary -->
</div>
<?php get_footer(); ?>
