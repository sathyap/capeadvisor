<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

<div id="primary">
	<div id="content" role="main">
		<div class="bodyContainer">
			
			<div id="residential">
			<?php $recent = new WP_Query("cat=3&showposts=6"); ?>	
			<?php //print_r($recent->posts[0]->ID); ?>
				<div class="leftContainer">
					
					<div class="leftbar"><?php echo get_the_post_thumbnail( $recent->posts[4]->ID, $size, $attr ); ?></div>
					<div class="leftbarimg2"><?php echo get_the_post_thumbnail($recent->posts[2]->ID, $size, $attr ); ?></div>
				</div>
				<div class="rightContainer">
					<div class="rightTop">
						<div class="rightbarimg1"><?php echo get_the_post_thumbnail($recent->posts[3]->ID, $size, $attr ); ?></div>
						<div class="rightbarDesc">
							<p><?php 
								$column_id = $recent->posts[5]->ID;
								$permalink = get_permalink($column_id);
								$column_page = get_post($column_id);
								echo $column_page->post_content; 
							?></p>
						</div>

					</div>
					
					<div class="rightBottom">
						<div class="rightbarimg2"><?php echo get_the_post_thumbnail($recent->posts[1]->ID, $size, $attr ); ?></div>
						<div class="rightbarimg3"><?php echo get_the_post_thumbnail($recent->posts[0]->ID, $size, $attr ); ?></div>
					</div>
				</div>
			</div>
			
			
			
			<!-- Commercial section start here -->
			<div id="commercial">
				<?php $recent = new WP_Query("cat=4&showposts=6"); ?>	
				<?php //print_r($recent->posts[0]->ID); ?>
				<div class="leftContainer">
					
					<div class="leftbar"><?php echo get_the_post_thumbnail( $recent->posts[5]->ID, $size, $attr ); ?></div>
					<div class="leftbarimg2"><?php echo get_the_post_thumbnail($recent->posts[3]->ID, $size, $attr ); ?></div>
				</div>
				<div class="rightContainer">
					<div class="rightTop">
						<div class="rightbarimg1"><?php echo get_the_post_thumbnail($recent->posts[4]->ID, $size, $attr ); ?></div>
						<div class="rightbarDesc">
							<p><?php 
								$column_id = $recent->posts[0]->ID;
								$permalink = get_permalink($column_id);
								$column_page = get_post($column_id);
								echo $column_page->post_content; 
							?></p>
						</div>

					</div>
					
					<div class="rightBottom">
						<div class="rightbarimg2"><?php echo get_the_post_thumbnail($recent->posts[2]->ID, $size, $attr ); ?></div>
						<div class="rightbarimg3"><?php echo get_the_post_thumbnail($recent->posts[1]->ID, $size, $attr ); ?></div>
					</div>
				</div>
			</div>
			<!-- Commercial section end here -->
			
			<!-- hospitality section start here -->
			<div id="hospitabilty">
				<?php $recent = new WP_Query("cat=5&showposts=6"); ?>	
				<?php //print_r($recent->posts[0]->ID); ?>
				<div class="leftContainer">
					
					<div class="leftbar"><?php echo get_the_post_thumbnail( $recent->posts[5]->ID, $size, $attr ); ?></div>
					<div class="leftbarimg2"><?php echo get_the_post_thumbnail($recent->posts[4]->ID, $size, $attr ); ?></div>
				</div>
				<div class="rightContainer">
					<div class="rightTop">
						<div class="rightbarimg1"><?php echo get_the_post_thumbnail($recent->posts[3]->ID, $size, $attr ); ?></div>
						<div class="rightbarDesc">
							<p><?php 
								$column_id = $recent->posts[0]->ID;
								$permalink = get_permalink($column_id);
								$column_page = get_post($column_id);
								echo $column_page->post_content; 
							?></p>
						</div>

					</div>
					
					<div class="rightBottom">
						<div class="rightbarimg2"><?php echo get_the_post_thumbnail($recent->posts[2]->ID, $size, $attr ); ?></div>
						<div class="rightbarimg3"><?php echo get_the_post_thumbnail($recent->posts[1]->ID, $size, $attr ); ?></div>
					</div>
				</div>
			</div>
			<!-- hospitality section end here -->
			
			
			
			
		</div>
	</div><!-- #content -->
</div><!-- #primary -->
<?php get_footer(); ?>
</div>
</div>

