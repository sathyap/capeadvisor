/*
 * jQuery Event Mousestop v0.1.1
 * http://richardscarrott.co.uk/posts/view/jquery-mousestop-event
 *
 * Copyright (c) 2010 Richard Scarrott
 * W/ thanks to Ben Alman for his jQuery special event API write up:
 * http://benalman.com/news/2010/03/jquery-special-events/
 *
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Requires jQuery v1.3+
 */

(function($) {
    // public vars
    $.mousestopDelay = 50;

    // special event
    $.event.special.mousestop = {
        setup: function(data) {
            $(this).data('mousestop', {delay: data})
                   .bind('mouseenter.mousestop', mouseenterHandler)
                   .bind('mouseleave.mousestop', mouseleaveHandler);
        },
        teardown: function() {
            $(this).removeData('mousestop')
                   .unbind('.mousestop');
        }
    };

    // private methods
    function mouseenterHandler() {
        if (typeof this.timeout === 'undefined') {
            this.timeout = null;
        }
        
        var elem = $(this),
            data = elem.data('mousestop'),
            delay = data.delay || $.mousestopDelay;

        elem.bind('mousemove.mousestop', function() {
            clearTimeout(this.timeout);
            this.timeout = setTimeout(function() {
                elem.trigger('mousestop');
                elem.unbind('mousemove.mousestop');
            }, delay);
        });
    };
    
    function mouseleaveHandler() {
        var elem = $(this);
        elem.unbind('mousemove.mousestop');
        clearTimeout(this.timeout);
    };

    // shorthand alias
    $.fn.mousestop = function(data, fn) {
        if (fn == null) {
            fn = data;
            data = null;
        }

        return arguments.length > 0 ? this.bind('mousestop', data, fn) : this.trigger('mousestop');
    };
})(jQuery);



 var fadeDuration=1800;
    var slideDuration=6000;
    var currentIndex=-1;
    var nextIndex=0, timer;
    $(document).ready(function()
    {    
         $('.align ul li:eq(0)').addClass('active');
          $('.first_set h2 a:eq(0)').removeClass('activer');  
           $('.first_set h2 a:eq(0)').addClass('activer');  
         $('.child-thumb:not(:first) .child-inner').css({opacity: 0.0});
	if($('.child-thumb').length > 1) {
        timer = setInterval('nextSlide()',slideDuration);
	}

      $('.align ul li a').bind('mousestop', function(e) {
   
        e.preventDefault();
         clearInterval(timer);
        var e= $(this), p= e.parent(), i=p.index();
p.addClass('active');
           nextSlide(i);  timer = setInterval('nextSlide()',slideDuration);   
      });

    $('.align ul li a').bind('click', function(e) {
   
        e.preventDefault();
         clearInterval(timer);
        var e= $(this), p= e.parent(), i=p.index();
		$('.child-thumb:eq('+i+')').find('.pretty_photo').trigger('click');

      }); 

 

    })
 
    function nextSlide(nextIndex){ 
    		
            if(typeof nextIndex === "undefined")
            nextIndex =currentIndex+1;
            

            if(nextIndex> ($('.child-thumb .child-inner').length-1))
            {
                nextIndex =0;
            }

              
             var temp = currentIndex;
              
            $('.child-thumb .child-inner:eq('+nextIndex+')').addClass('show').animate({opacity: 1.0,}, fadeDuration,
            function() {
              $('.child-thumb:eq('+nextIndex+')').css({zIndex: 101});
              $('.child-thumb .child-inner:eq('+temp+')').removeClass('show').animate({opacity: 0.0,}, 100,
            function() { 

                      two(temp)

            });
           
           
                $('.align ul li').removeClass('active');
                $('.align ul li:eq('+nextIndex+')').addClass('active'); 
                one(nextIndex, temp);         

            });
          
            currentIndex = nextIndex;
    } 
