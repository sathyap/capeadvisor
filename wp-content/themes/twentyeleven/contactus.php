<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
   Template Name: contactus	
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">
<div class="bodyContainer">
					<div class="leftContainer">
					<div class="leftbars"><img src="<?php bloginfo('template_url'); ?>/images/contactus1.jpg" alt="contact" ></div>
					<div class="leftbarimgs"><img src="<?php bloginfo('template_url'); ?>/images/contactus3.jpg"  alt="contact"/></div>
				</div>
				<div class="rightContainer">
					<div class="profileDesc">
						<h1><?php the_title(); ?></h1>
						<br clear="all"/>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', 'page' ); ?>
						<?php endwhile; // end of the loop. ?>
					</div>
					<div class="contactDiv">
					<div class="rightbarimgs"><img src="<?php bloginfo('template_url'); ?>/images/contactus2.jpg" alt="contact"/></div>
					<div class="profileDesc1">
						<h4><b>Satellite Office:</b></h4>
						<div class="contentArea">
							<p><?php $key="CompanyName"; echo get_post_meta($post->ID, $key, true); ?></p>
							<p><?php $key="Address1"; echo get_post_meta($post->ID, $key, true); ?></p>
							<p><?php $key="Address2"; echo get_post_meta($post->ID, $key, true); ?></p>
							<p><?php $key="Telephone"; echo get_post_meta($post->ID, $key, true); ?></p>
							<p><?php $key="Fax"; echo get_post_meta($post->ID, $key, true); ?></p>
						</div>
					</div>
					</div>
				</div>
			</div><!-- #content -->
		</div><!-- #primary -->
</div>
<?php get_footer(); ?>
