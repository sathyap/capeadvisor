<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
   Template Name: profile	
 */

get_header(); ?>
<style>
.rightbarimgs {background-color: #63708A;}
.rightbarimgs img {float:right;}
</style>
		<div id="primary">
			<div id="content" role="main">
<div class="bodyContainer">
					<div class="leftContainer">
					<div class="leftbars"><img src="<?php the_field('left_image_1'); ?>" alt="profile" /></div>
					<div class="leftbarimgs"><img src="<?php the_field('left_image_2'); ?>" alt="profile" /></div>
				</div>
				<div class="rightContainer">
					<div class="profileDesc">
						<h1><?php the_title(); ?></h1>	
						<br clear="all"/>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', 'page' ); ?>
						<?php endwhile; // end of the loop. ?>
					</div>
					<div class="profileBottom">
					<div class="rightbarimgs"><img src="<?php the_field('image'); ?>" alt="profile" /></div>
					<div class="profileDesc1">

			<!--	<?php echo get_field( "description", $pageChild->ID ); ?>		-->
	<?php	//the_secondary_content();?>

		<?php echo get_post_meta( get_the_ID(), '_secondary_html_802', true ); ?>
					</div>
					</div>
				</div>
			</div><!-- #content -->
		</div><!-- #primary -->
</div>

<?php get_footer(); ?>
