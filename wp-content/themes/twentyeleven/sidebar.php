<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

$options = twentyeleven_get_theme_options();
$current_layout = $options['theme_layout'];

?>

		<div class="left_contents_one">		      	  
			<?php if(is_page('hotels')) {
				echo '<style>.custom_img {top:0px;width:100%;}</style>';	
				}
			if(is_page('office')) {
				//echo '<style>.custom_img{top:378px;}</style>';	
				echo '<style>.custom_img{top:-46px;}</style>';	
				}
			if(is_page('residentials')) {
				//echo '<style>.custom_img {top:245px;}</style>';	
				echo '<style>.custom_img {top:-179px;}</style>';	
				}
			if(is_page('retail')) {
				//echo '<style>.custom_img {top:424px;}</style>';	
				echo '<style>.custom_img {top:0px;}</style>';	
				}
			if(is_page('other-businesses')) {
				//echo '<style>.custom_img {top:220px;}</style>';	
				echo '<style>.custom_img {top:-204px;}</style>';	
				}

			?>



		  <div class="align">
       <h1 class="entry-title_one"> <?php echo $title1=get_the_title($ID); ?> </h1>  
<?php $one= get_the_title($pageId) ?>
<?php 
if(wp_list_pages('child_of='.$post->ID.'&echo=0&depth=1')) {
echo "<ul>";
wp_list_pages('title_li=&child_of='.$post->ID.'&depth=1');
echo "</ul>";
}
/* elseif(get_the_title($post->post_parent) != the_title(' ' , ' ',false)) {
echo "<ul>";
wp_list_pages('child_of='.$post->post_parent.'&title_li=&exclude=2');
echo "</ul>";
} */
?>


    </div>
    <div class="left_contents_two">
    		  	<div style="position:relative;clear:both;">
    		  	
    		  	
    		  	<?php if(is_page('hotels')) { ?> 
    		  	<div class="custom_hotelsImg"> <?php } else if(is_page('office')) { ?> 
    		  	<div class="custom_ofcImg"> <?php } else if(is_page('residentials')) { ?> 
    		  	<div class="custom_resiImg"> <?php } else if(is_page('retail')) { ?> 
    		  	<div class="custom_retImg"> <?php } else if(is_page('restaurants-nightlife')) { ?> 
    		  	<div class="custom_restImg"> <?php } else if(is_page('other-businesses')) { ?>
    		  	<div class="custom_otherImg"> <?php } else { ?>
    		  	
    		  	<div class="custom_img"><?php } ?><a href="javascript:void(0);" ><img src="<?php the_field('image'); ?>"  width="" height="" alt="Slider Image"/></a></div> </div> 
    
	      
		  </div>
		  
		  </div>
		
